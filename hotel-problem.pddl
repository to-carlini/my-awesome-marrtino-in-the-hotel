(define (problem hotel-problem) 
    (:domain hotel-domain)
    (:objects
        bot        
        pos1        pos2        pos3        pos4        pos5        pos6
        room1        room2        room3        room4        room5        room6
        )
    (:init 
        (near room1 pos1) (near room1 room2) 
        (near room2 room1) (near room2 room3) (near room2 pos2) 
        (near room3 room2) (near room3 pos3)
        (near room4 room5) (near room4 pos4) 
        (near room5 room4) (near room5 room6) (near room5 pos5)
        (near room6 room5) (near room6 pos6)
        (near pos1 room1) (near pos1 pos2) (near pos1 pos4)
        (near pos2 room2) (near pos2 pos1) (near pos2 pos3) (near pos2 pos5)
        (near pos3 room3) (near pos3 pos2) (near pos3 pos6)
        (near pos4 room4) (near pos4 pos1) (near pos4 pos5)
        (near pos5 room5) (near pos5 pos4) (near pos5 pos2) (near pos5 pos6)
        (near pos6 room6) (near pos6 pos5) (near pos6 pos3)

        (room room1) (room room2) (room room3) (room room4) (room room5) (room room6)
        (pos pos1) (pos pos2) (pos pos3) (pos pos4) (pos pos5) (pos pos6)
        (door room1) (door room2) (door room3) (door room4) (door room5) (door room6)
        (people room1) (people room3) (people room4) (people room5)
        (dirty room2) (dirty room4) (dirty room6)
        (robot bot)
        (at bot pos1)
    )
    (:goal 
        (and    (not (dirty room2)) 
                (not (dirty room6)))
    )
)